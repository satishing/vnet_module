output "resource_group_id" {
  description = "The id of the resource group"
  value       = azurerm_resource_group.this.id
}

output "virtual_network_id" {
  description = "The id of the virtual network"
  value       = azurerm_virtual_network.this.id
}