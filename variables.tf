variable "resource_group_name" {
  description = "Name of the resource group"
  type        = string
}

variable "location" {
  description = "Location of the resource"
  type        = string
}

variable "vnet_name" {
  description = "Name of the virtual network"
  type        = string
}

variable "vnet_address_space" {
  description = "Address spaces of the virtual network"
  type        = list(string)
}

variable "tags" {
  description = "Tags of the resource"
  type        = map(any)
  default     = {}
}