module "vnet" {
  source = "./"

  # Required variables
  location            = "eastus"
  resource_group_name = "rg-project-alpha"
  vnet_address_space  = ["10.0.0.0/16"]
  vnet_name           = "vnet-project-alpha"

  # Optional variables
  tags = {
    Owner = "Infra Team"
  }
}

output "resource_group_id" {
  value = module.vnet.resource_group_id
}

output "virtual_network_id" {
  value = module.vnet.virtual_network_id
}